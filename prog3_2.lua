print("Assignment #3-2, Aaron Hayag, aaron_hayag@yahoo.com")

function InfixToPostfix(str)
    result = " "
     
    tokenArray = tokenize(str)
    stack = {}
    stackSize = 0
    outputQueue = {}

    for i = 1, getNumTokens(tokenArray) do
        if isOperand(tokenArray[i]) then  --if token is an operand put it in output
            table.insert(outputQueue, tokenArray[i])
        else  --if token is an operator and stack is empty, put it in stack
            if stackSize == 0 then  
                table.insert(stack, tokenArray[i])
                stackSize = stackSize + 1
            else  --if token is an operator compare precedence with tokens in stack
                str1 = tokenArray[i]  --current operator
                str2 = stack[stackSize] --operator on top of stack
                while(stackSize ~= 0 and isLowerPrecedence(str1, str2)) do --while current operator is lower or equal precedence than top operator
                    table.insert(outputQueue, stack[stackSize])  --insert top operator of stack in output
                    table.remove(stack)  --pop top operator of stack
                    stackSize = stackSize - 1
                    str2 = stack[stackSize]  --top operator on stack is updated
                end
                table.insert(stack, tokenArray[i])  --push current operator in stack
                stackSize = stackSize + 1
            end
        end  
    end

    while(stackSize ~= 0) do  --pop remaining operators in stack and put it in output
        table.insert(outputQueue, stack[stackSize])
        stackSize = stackSize - 1
    end
       
    result = (table.concat(outputQueue, " "))   
    
    return result
end

function isLowerPrecedence(str1, str2)  --returns true if str1 is lower or equal precedence than str2
    if (str1 == "+" or str1 == "-") and (str2 == "+" or str2 == "-") then
        return true
    elseif (str1 == "+" or str1 == "-") and (str2 == "*" or str2 == "/") then
        return true
    elseif (str1 == "*" or str1 == "/") and (str2 == "*" or str2 == "/") then
        return true
    else
        return false
    end
end
        
function tokenize(str)
    tokenArray = {}

    for i in string.gmatch(str, '([^" "]+)') do
        table.insert(tokenArray, i)
    end

    return tokenArray
end


function getNumTokens(table)
    count = 0

    for k, v in pairs(table) do
        count = count + 1
    end

    return count
end

function isOperand(str)
    if str == "+" or str == "-" or str == "*" or str == "/" then
        return false
    else
        return true
    end
end
