#include <iostream>
#include <string>
#include "lua.hpp"
#include "lualib.h"
#include "lauxlib.h"


using namespace std;

int main(int argc, char* argv[]){
    cout << "Assignment #3-3, Aaron Hayag, aaron_hayag@yahoo.com" << endl;
    string input;
    getline(cin, input);
    const char* cstr = input.c_str();

    lua_State *L = luaL_newstate();
    luaL_openlibs(L);
    luaL_dofile(L, argv[1]);
    lua_getglobal(L, "InfixToPostfix");
    lua_pushstring(L, cstr); //push arguments on to the stack
    lua_pcall(L, 1, 1, 0);  //lua state, # of arguemnts, # of returns, # of error functions

    cout << lua_tostring(L, -1)<< endl;  //print return values 
    lua_close(L);

    return 0;
}