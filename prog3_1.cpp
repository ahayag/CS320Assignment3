#include <iostream>
#include "lua.hpp"
#include "lualib.h"
#include "lauxlib.h"

using namespace std;

int main(int argc, char* argv[]){
    cout << "Assignment #3-1, Aaron Hayag, aaron_hayag@yahoo.com" << endl;

    lua_State *L = luaL_newstate();  //create new lua state
    luaL_openlibs(L);  //open lua libraries
    luaL_dofile(L, argv[1]);  //runs command line argument which is the name of lua file
    lua_close(L);

    return 0;
}