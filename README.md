Aaron Hayag aaron_hayag@yahoo.com

CS320 Assignment 3

prog3_1
    Program 3-1 is a C++ program that takes the name of a lua file as a single command line argument. It will then execute the lua file
    in a lua environment

prog3_2
    Program 3-2 is a lua file with an Infix to Postfix function. It takes an input string, tokenizes the string, and returns the postfix
    form of the string. It uses the Infix to Postfix algorithm that uses a stack to compare precedence between oeprators. 

prog3_3
    Program 3-3 is a C++ program that creates a lua environment and runs the name of a lua file as a single command line argument. It gets
    input from stdin and calls the lua InfixToPostfix() function from prog3_2. It then prints the resulting postfix string.
    
    Compilation:
    g++ prog3_3.cpp -o prog3_3 -I lua-5.3.4/src -L lua-5.3.4/src -l lua -l m -l dl
    
    Execution:
    /prog3_3 prog3_2.lua